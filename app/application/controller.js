import Ember from 'ember';

export default Ember.Controller.extend({
	SesionIniciada: localStorage.sesionActiva,



	actions: {
		logout: function () {
			this.set('SesionIniciada', false);
			// console.log(this.store.peekRecord('home',1));
			var home = this.controllerFor('home');
			var homeTobR = home.findProperty ('id', 1);
			homeTobR.deleteRecord();
			homeTobR.save();
			this.transitionToRoute('home');
		}
	}
});


